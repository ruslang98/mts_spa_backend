from django.urls import path
from rest_framework import routers

from .views import (
    sign_in, sign_up,
    UserViewSet,
    ConveyorListView,
    ConveyorDetailView,
)

app_name = 'app'

urlpatterns = [
    path('auth/signin/', sign_in),
    path('auth/signup/', sign_up),
    path('conveyors/', ConveyorListView.as_view()),
    path('conveyors/<int:pk>/', ConveyorDetailView.as_view()),
]

router = routers.SimpleRouter()
router.register(r"users", UserViewSet, basename='users')
# router.register(r"^conveyors", ConveyorListView, basename='conveyors')

urlpatterns += router.urls
