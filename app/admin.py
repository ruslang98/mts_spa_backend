from django.contrib import admin
from .models import Conveyor, User


class ConveyorAdmin(admin.ModelAdmin):
    list_display = ('number',
                    'location',
                    'price',
                    'capacity',
                    'radius',
                    'node',
                    'size',
                    'status',
                    'organization',
                    'date',
                    )

    list_filter = ('number',
                   'location',
                   'price',
                   'capacity',
                   'radius',
                   'node',
                   'size',
                   'status',
                   'organization',
                   'date',
                   )


admin.site.register(Conveyor, ConveyorAdmin)
admin.site.register(User)
