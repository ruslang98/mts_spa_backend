import transliterate

from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.parsers import MultiPartParser

from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend

from .custom_backend import CustomBackend

from .models import User
from .models import Conveyor

from .serializers import UserSerializer
from .serializers import ConveyorSerializer


@api_view(["POST"])
def sign_in(request):
    email = request.data.get("email")
    password = request.data.get("password")

    if not (email and password):
        return Response(
            data={"detail": "Need dictionary {email:<email>, password:<password>}"},
            status=400,
        )
    custom_backend = CustomBackend()
    user = custom_backend.authenticate(email=email, password=password)
    if not user:
        return Response(data={"detail": "Email or password incorrect"}, status=401)

    return Response(data={"authorization_token": user.auth_token.key, "id": user.id,
                          "departament": user.departament}, status=200)


@api_view(["POST"])
def sign_up(request):
    """
           Create user.

           Request and response examples:
          ```
          User departament:
          "ZO" - ZERO - Нулевой уровень
          "LS" - LOGISTICS - Отдел логистики
          "TL" - TECHNICAL - Технический отдел
          "DT" - DEVELOPMENT - Отдел разработки
           Default: Logistics
          Response value:
            {
                "email": "admin@mail.ru",
                "password": "admin",
                "first_name": "Антуан",
                "last_name": "Хуан",
                "patronymic": "Олегович"
            }

    """

    first_name = request.data.get("first_name")
    last_name = request.data.get("last_name")
    password = request.data.get("password")
    patronymic = request.data.get("patronymic")
    email = request.data.get("email")

    if not (first_name and last_name and password and email):
        return Response(
            data={
                "detail": "Need dictionary {first_name:<first_name>, last_name:<last_name>, email:<email>, "
                          "password:<password>} "
            },
            status=406,
        )

    if User.objects.filter(email=email).exists():
        return Response(data={"detail": "This user already exists"}, status=400)

    email_validator = EmailValidator()
    try:
        email_validator(email)
    except ValidationError:
        return Response(
            data={"detail": "{} has validation error".format(email)}, status=406
        )
    new_user = User.objects.create_user(
        email=email,
        password=password,
        first_name=first_name,
        last_name=last_name,
        patronymic=patronymic
    )
    return Response(data={"authorization_token": new_user.auth_token.key, "id": new_user.id}, status=201)


class UserViewSet(ViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = User.objects.filter(is_active=True)

        serializer = UserSerializer(queryset, many=True)

        return Response({'users': serializer.data})

    def create(self, request):
        data = request.data.get("user")
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        patronymic = data.get("patronymic")
        password = data.get("password")
        email = data.get("email")

        if not (first_name, last_name, patronymic, password, email):
            return Response(status=401)

        new_user = User.objects.create_user(
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
            patronymic=patronymic
        )

        return Response({"id": new_user.id}, status=201)

    def update(self, request, pk):

        """
               Update user.

               Request and response examples:
              ```
              Response value:
                {
                "user":
                    {
                    "first_name": "Олег"
                 }
                }

                ```
                """

        try:
            user = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(data={"detail": "User not found"}, status=404)

        data = request.data.get("user")
        serializer = UserSerializer(
            instance=user, data=data, partial=True
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response(serializer.data, status=201)

    def retrieve(self, request, pk):
        try:
            queryset = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(data={"detail": "User not found"}, status=404)
        serializer = UserSerializer(queryset)
        return Response(serializer.data)

    def destroy(self, request, pk):
        try:
            queryset = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(status=404)
        queryset.delete()

        return Response(status=204)

    @action(detail=True, methods=['patch'], parser_classes=[MultiPartParser])
    def set_avatar(self, request, pk):
        """
        Set avatar. You need to download the avatar file and specify it in the headers.

        Response value:
        ```
        Example header string:
        Content-Type multipart/form-data
        Authorization Token 51bfef57318703720118e30ca80fb855fa12c9fd
        Content-Disposition attachment; filename=avatar.jpg
        ```
        """
        avatar = request.FILES.get('avatar')

        if not avatar:
            return Response(
                data={'detail': 'Image file required!'},
                status=400
            )
        try:
            avatar._name = '_'.join(transliterate.translit(avatar._name, reversed=True).split()).replace("'", '')
        except:
            pass
        if avatar.name.split('.')[-1] not in {'jpg', 'png'}:
            return Response(
                data={'detail': 'Image must be jpg or png!'},
                status=400
            )

        user = User.objects.get(pk=pk)
        user.avatar = avatar
        user.save()
        return Response(
            status=201,
            data={
                "avatar": user.avatar.url
            }
        )


# class ConveyorViewSet(ViewSet):
#     # authentication_classes = [TokenAuthentication]
#     # permission_classes = [IsAuthenticated]
#     filter_backends = (filters.DjangoFilterBackend,)
#     filterset_class = ConveyorFilter
#
#     def create(self, request):
#         """
#                Create сonveyor.
#
#                Request and response examples:
#               ```
#               Conveyor statuses:
#               "ZO" - ZERO - Нулевой отдел
#               "LS" - LOGISTICS - Отдел логистики
#               "TL" - TECHNICAL - Технический отдел
#               "DT" - DEVELOPMENT - В разработке
#               Response value:
#                 {
#                 "conveyor":
#                     {
#                     "number": "25489",
#                     "location": "Краснодар",
#                     "price": "65000",
#                     "capacity": "2524",
#                     "radius":"12432",
#                     "node": "19867",
#                     "size": "95634",
#                     "status": "LS",
#                     "organization": "МТС",
#                     "date": "2020-05-15",
#                     "users": [
#                       1,
#                       2,
#                       3
#                     ],
#                     "description": "Большое красивое поле"
#                  }
#                 }
#
#         """
#         queryset = request.data.get("conveyor")
#         if not queryset:
#             return Response(status=400)
#         serializer = ConveyorSerializer(data=queryset)
#         if serializer.is_valid(raise_exception=True):
#             serializer.save()
#         return Response(serializer.data, status=201)
#
#     def list(self, request):
#         queryset = Conveyor.objects.all()
#         pagination = PageNumberPagination()
#         pagination_queryset = pagination.paginate_queryset(queryset, request)
#         serializer = ConveyorSerializer(pagination_queryset, many=True)
#         response = pagination.get_paginated_response(serializer.data)
#         return response
#
#     def retrieve(self, request, pk):
#         try:
#             queryset = Conveyor.objects.get(pk=pk)
#         except Conveyor.DoesNotExist:
#             return Response(data={"detail": "Conveyor not found"}, status=404)
#         serializer = ConveyorSerializer(queryset)
#         return Response(serializer.data)
#
#     def update(self, request, pk):
#         """
#                Update сonveyor.
#
#                Request and response examples:
#               ```
#               Conveyor statuses:
#               "LS" - LOGISTICS - Отдел логистики
#               "TL" - TECHNICAL - Технический отдел
#               "DT" - DEVELOPMENT - В разработке
#               Response value:
#                 {
#                 "conveyor":
#                     {
#                     "number": "5632",
#                     "location": "Краснодар",
#                     "price": "1453",
#                     "capacity": "2524",
#                     "radius":"12432",
#                     "node": "19867",
#                     "description": "Маленькое некрасивое поле"
#                  }
#                 }
#
#         """
#         partner = get_object_or_404(Conveyor.objects.all(), pk=pk)
#         data = request.data.get("conveyor")
#         serializer = ConveyorSerializer(instance=partner, data=data, partial=True)
#         if serializer.is_valid(raise_exception=True):
#             serializer.save()
#         return Response(serializer.data, status=200)
#
#     def destroy(self, request, pk):
#         try:
#             queryset = Conveyor.objects.get(pk=pk)
#         except Conveyor.DoesNotExist:
#             return Response(status=404)
#         queryset.delete()
#         return Response(status=204)

class ConveyorFilter(filters.FilterSet):
    price = filters.RangeFilter()
    size = filters.RangeFilter()

    class Meta:
        model = Conveyor
        fields = ['location', 'price', 'capacity', 'radius', 'node', 'size', 'organization', 'date']


class ConveyorListView(ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Conveyor.objects.all()
    serializer_class = ConveyorSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ConveyorFilter


class ConveyorDetailView(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Conveyor.objects.all()
    serializer_class = ConveyorSerializer







