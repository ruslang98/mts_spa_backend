from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager

from rest_framework.authtoken.models import Token


def get_avatar_path(instance, filename):
    return "avatars/{}/{}".format(instance.email, filename)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        Token.objects.create(user=user)
        return user

    def create_user(self, email, password=None, **extra_fields):

        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    ZERO = 'ZO'
    LOGISTICS = 'LS'
    TECHNICAL = 'TL'
    DEVELOPMENT = 'DT'

    DEPARTAMENT_CHOICES = [
        (ZERO, 'Zero'),
        (LOGISTICS, 'Logistics'),
        (TECHNICAL, 'Technical'),
        (DEVELOPMENT, 'Development'),
    ]

    email = models.EmailField(unique=True)
    city = models.CharField(max_length=30, null=True, blank=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    patronymic = models.CharField(max_length=30, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_confirm = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=True)
    avatar = models.ImageField(null=True, blank=True, upload_to=get_avatar_path)
    departament = models.CharField(max_length=30, choices=DEPARTAMENT_CHOICES, default=ZERO)
    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Conveyor(models.Model):
    ZERO = 'ZO'
    LOGISTICS = 'LS'
    TECHNICAL = 'TL'
    DEVELOPMENT = 'DT'

    STATUS_CHOICES = [
        (ZERO, 'Zero'),
        (LOGISTICS, 'Logistics'),
        (TECHNICAL, 'Technical'),
        (DEVELOPMENT, 'Development'),

    ]

    number = models.IntegerField(verbose_name='Номер конвейера')
    location = models.CharField(max_length=30, verbose_name='Местоположение')
    price = models.IntegerField(verbose_name='Цена')
    capacity = models.IntegerField(verbose_name='Емкость')
    radius = models.IntegerField(verbose_name='Радиус видимости')
    node = models.IntegerField(verbose_name='Количество узлов')
    size = models.IntegerField(verbose_name='Размер')
    status = models.CharField(max_length=30, choices=STATUS_CHOICES, default=ZERO, verbose_name='Статус')
    organization = models.CharField(max_length=30, verbose_name='Обслуж. организация')
    date = models.DateField(verbose_name='Дата')
    users = models.ManyToManyField(User, related_name='favorite_posts', blank=True, verbose_name='Избранное для', )
    description = models.TextField(verbose_name='Описание')
