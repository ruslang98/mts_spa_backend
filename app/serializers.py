from rest_framework import serializers

from .models import User
from .models import Conveyor


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(format="%Y.%m.%d %H:%M:%S")

    class Meta:
        model = User
        fields = [
            "id",
            "email",
            "first_name",
            "last_name",
            "patronymic",
            "date_joined",
            "is_confirm",
            "avatar",
            "departament",
        ]


class ConveyorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conveyor
        fields = ["id",
                  "number",
                  "location",
                  "price",
                  "capacity",
                  "radius",
                  "node",
                  "size",
                  "status",
                  "organization",
                  "date",
                  "date",
                  "users",
                  "description",
        ]
